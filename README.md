# tabGAP - tabulated Gaussian approximation potentials

Tools to make and run tabulated (faster) versions of GAP machine-learning interatomic potentials trained with low-dimensional descriptors (2-body, 3-body, EAM density). See more details in Refs. [1, 2].

## How to:
####  Create a tabulated GAP potential file

 - Train a GAP using any combination of 2-body, 3-body and EAM density descriptors using [QUIP](https://github.com/libAtoms/QUIP) (NOTE: for the EAM-density descriptor, you need the GAP source from https://github.com/jesperbygg/GAP branch `eam_density_multi`).

 - Make tabulated potential files. Run [tabulate/tabgap.py](tabulate/tabgap.py) directly (see `--help` for details), or write something like:
    ```
    from tabulate.tabgap import tabGAP

    tabgap = tabGAP('/path/to/gapfile.xml',
                    n2b=1000, n3b=(60, 60, 60), neam=1000)
    tabgap.compute_energies(ncores=3)
    tabgap.write_potential_files()
    ```
    Note: computing the 3b grid will take several minutes per element triplet. See [examples](examples) for more help.

####  Run a [LAMMPS](https://lammps.org) simulation

 - Requires lammps version 24 March 2022 or newer!

 - Copy the files in [lammps](lammps) into your `lammps/src/` folder and compile normally.
 - Use the `pair_style tabgap` as for example:
    ```
    pair_style   tabgap
    pair_coeff   * * W.tabgap W yes yes
    ```
    for a 2b+3b W potential. Or:
    ```
    pair_style   hybrid/overlay tabgap eam/fs
    pair_coeff   * * tabgap W.tabgap W no yes
    pair_coeff   * * eam/fs W.eam.fs W
    ```
    for a 2b+3b+EAM potential.

## References

If you use this code to run or create a tabGAP potential, please cite:

[1] J. Byggmästar, K. Nordlund, and F. Djurabekova, *Simple machine-learned interatomic potentials for complex alloys*, Phys. Rev. Materials **6**, 083801 (2022), https://doi.org/10.1103/PhysRevMaterials.6.083801, https://arxiv.org/abs/2203.08458

[2] J. Byggmästar, K. Nordlund, F. Djurabekova, *Modeling refractory high-entropy alloys with efficient machine-learned interatomic potentials: Defects and segregation*, Phys. Rev. B **104**, 104101 (2021), https://doi.org/10.1103/PhysRevB.104.104101, https://arxiv.org/abs/2106.03369

## Potentials

Potential files for some tabGAPs are provided in the [potentials](potentials) folder. If you use any of the potentials, please cite the corresponding reference.

## TODO

 - merge eam/fs and tabgap potfiles & support nband-EAM in pair_tabgap
