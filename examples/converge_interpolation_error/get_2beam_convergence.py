from tabulate.tabgap import tabGAP
from tabulate.utils import get_interpolation_error

# Create tabGAPs with increasingly dense 2b and eam grids to
# check convergence of interpolation error.

gap_xml = '../test_files/gap_W-Ta_example_2b+eam.xml'
xyz_file = '../test_files/db_W-Ta_example_no_isolated.xyz'

tabgap = tabGAP(gap_xml)

print('# n, E (meV/atom), F (eV/Å)')
for n in [20, 100, 200, 500, 1000, 2000, 5000]:
    tabgap.set_neam(n)  # NOTE: also updates n2b
    tabgap.compute_energies(ncores=3, save_data=False)
    tabgap.write_potential_files()
    rmse = get_interpolation_error(xyz_file, tabgap.pair_style,
                                   tabgap.pair_coeff, gap_xml=gap_xml,
                                   compute_gap=True)
    print(n, rmse['energy'], rmse['force'], flush=True)
