from tabulate.tabgap import tabGAP

gap_xml = '../test_files/gap_W-Ta_example_2b+3b.xml'

verbose = True
tabgap = tabGAP(gap_xml, n2b=2000,
                n3b=(60, 60, 60),
                verbose=verbose)
tabgap.compute_energies(ncores=3, verbose=verbose)
tabgap.write_potential_files(verbose=verbose)
