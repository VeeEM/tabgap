from tabulate.tabgap import tabGAP

gap_xml = '../test_files/gap_W-Ta_example_2b.xml'

verbose = True
tabgap = tabGAP(gap_xml, n2b=2000, verbose=verbose)
tabgap.compute_energies(ncores=2, verbose=verbose)
tabgap.write_potential_files(verbose=verbose)
