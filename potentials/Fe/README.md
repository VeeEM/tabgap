Use as:

```
mass 1 55.845
pair_style hybrid/overlay eam/fs tabgap
pair_coeff * * eam/fs /path/to/Fe-2021-04-06.eam.fs Fe
pair_coeff * * tabgap /path/to/Fe-2021-04-06.tabgap Fe no yes
```
