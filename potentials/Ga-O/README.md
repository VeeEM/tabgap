Download the tabGAP files from: https://doi.org/10.6084/m9.figshare.21731426

Use as:

```
mass 1 69.723
mass 2 15.999
pair_style hybrid/overlay eam/fs tabgap
pair_coeff * * eam/fs Ga-O.eam.fs Ga O
pair_coeff * * tabgap Ga-O.tabgap Ga O no yes
```
