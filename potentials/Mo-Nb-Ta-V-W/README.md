# Version 1 (2b+3b tabGAP)

Download potential file from https://doi.org/10.23729/1b845398-5291-4447-b417-1345acdd2eae

Use as:

```
pair_style      tabgap
pair_coeff      * * /path/to/Mo-Nb-Ta-V-W.tabgap Mo Nb Ta V W yes yes
```


# Version 2 (2b+3b+EAM tabGAP)

Download the two potential files from https://doi.org/10.23729/1e6d0215-d26b-4f5f-8f5b-df575efa6594 (tabgap_potential_files/2b+3b+EAM/Mo-Nb-Ta-V-W.*)

Use as:

```
pair_style      hybrid/overlay tabgap eam/fs
pair_coeff      * * tabgap /path/to/Mo-Nb-Ta-V-W.tabgap Mo Nb Ta V W no yes
pair_coeff      * * eam/fs /path/to/Mo-Nb-Ta-V-W.eam.fs Mo Nb Ta V W
```