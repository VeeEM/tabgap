import numpy as np
import scipy.optimize
import os
from tabulate.quip_tools import *
from tabulate.spline_tools import *
from tabulate.write import *
from tabulate.periodic_table import *

# eam/fs potential tabulation tools (embedding energy only)


def generate_grid_points(density_min, density_max, n):
    """  """
    return np.linspace(density_min, density_max, n)


def set_missing_default_hyperparameters(parameters):
    """ default values set in QUIP """

    defaults = {'mode': 'blind',
                'pair_function': 'FSgen',
                'order': '3',
                'rmin': '0',
               }
    for key, value in defaults.items():
        if key not in parameters.keys():
            parameters[key] = value
    return parameters


def get_pair_density_kwargs(descriptor_dict):
    """ helper function to get needed hyperparameters from descriptor dict """
    parameters = set_missing_default_hyperparameters(descriptor_dict)
    rcut = float(parameters['cutoff'])
    rmin = float(parameters['rmin'])
    order = int(parameters['order'])
    function = parameters['pair_function']
    mode = parameters['mode']
    kwargs = {'rcut': rcut, 'rmin': rmin,
              'mode': mode, 'function': function, 'order': order}
    return kwargs


def pair_density(r, element_pair, rcut, rmin=0, mode='blind', function='FSgen', order=3):
    """ pairwise density functions implemented in QUIP/GAP """

    Zi = atomic_numbers[element_pair[0]]
    Zj = atomic_numbers[element_pair[1]]
    if mode == 'blind':
        prefactor = 1.0
    elif mode == 'EAM':
        prefactor = np.sqrt(Zj) / 10.0
    elif mode == 'FSsym':
        prefactor = np.sqrt(Zj * Zi) / 40.0
    elif mode == 'FSgen':
        prefactor = np.sqrt(Zi * Zj) / 10.0 / Zi**0.4
    else:
        raise NotImplementedError(f'unrecognised eam_density mode {mode}')

    if function == 'FS':
        rho = (r - rcut)**2
        rho_rmin = (rmin - rcut)**2
    elif function == 'FSgen':
        n = order
        rho = (-1)**n * (r - rcut)**n / rcut**n
        rho_rmin = (-1)**n * (rmin - rcut)**n / rcut**n
    elif function == 'polycutoff':
        chi = (r - rmin) / (rcut - rmin)
        rho = 1 - chi**3 * (6.0*chi**2 - 15*chi + 10.0)
        rho_rmin = 1.0
    elif function == 'coscutoff':
        rho = 0.5 * (1 + np.cos(np.pi * (r - rmin) / (rcut - rmin)))
        rho_rmin = 1.0
    else:
        raise NotImplementedError(f'Unrecognised pair density function {function_name}')

    rho *= prefactor
    rho_rmin *= prefactor

    return np.where(r < rmin, rho_rmin, np.where(r > rcut, 0, rho))


def pair_density_inverse(density, element_pair, rcut, rmin=0, mode='blind',
                         function='FSgen', order=3):
    """ Return distance r that gives the desired pair density contribution.
    Uses analytical inverse function for FS-like polynomials and numerical
    scipy root-finder otherwise """

    # TODO support array input

    do_numerically = False

    if density < 0:
        raise ValueError('negative pair density not allowed!')

    if function == 'FSgenTODO':
        # TODO mode
        r = rcut * (1 - density**(1 / order))
    else:
        do_numerically = True
        def find_numerical_root(rr, rho):
            return rho - pair_density(rr, element_pair, rcut, rmin, mode, function, order)

    if do_numerically:
        result = scipy.optimize.root_scalar(find_numerical_root, (density), method='bisect',
                                            bracket=(rmin, rcut), x0=2.0)
        r = result.root
        # print(density, r)

    if r > rcut or r < 0:
        raise ValueError(f'Obtained distance {r} not in [0, rcut] interval for density {density}!')

    return r


def get_total_density_atom(positions, element_pair, rcut, rmin=0, mode='blind',
                           function='FSgen', order=3, i=0):
    """" Compute total density of atom index i for (xml-parsed) pair density function
    and parameters.
    Positions should be array or list-of-list: [(x1, y1, z1), (x2, y2, z2), ...]
    NOTE: does NOT take pbc minimum image convention into account!! (no need here) """

    positions = np.asarray(positions)
    rij = np.linalg.norm(positions[i] - positions, axis=1)
    rij = np.delete(rij, i)
    pair_densities = pair_density(rij, element_pair, rcut, rmin, mode, function, order)
    return np.sum(pair_densities)


def get_embedding_atom_positions(density, element_pair, parameters, tolerance=1e-6):
    """ Place atoms around a central atom so that it gets the desired total eam density.
    Return list of coordinates """

    kwargs = get_pair_density_kwargs(parameters)

    # place atoms at this distance until we approach desired density
    default_distance = 0.2 * kwargs['rcut']
    density_increment_atom = pair_density(default_distance, element_pair, **kwargs)
    N_neighbours = int(np.ceil(density / density_increment_atom))

    density_last_atom = density - density_increment_atom * (N_neighbours - 1)
    if density_last_atom < 0:
        raise Exception(f'need negative density {density_last_atom} for last atom, bug !?!?')

    positions = [np.array([0, 0, 0])]

    # add atoms to reach density. For zero density, N_neighbours = 0 so no atoms will be added.
    if N_neighbours > 1:
        positions.append(np.array([default_distance, 0, 0]))
        for i in range(N_neighbours - 2):
            # rotate previous vector in 2D by small angle to avoid overlap
            a = 0.02
            x1, y1 = positions[-1][0], positions[-1][1]
            new_pos = np.array([x1*np.cos(a) - y1*np.sin(a), x1*np.sin(a) + y1*np.cos(a), 0])
            positions.append(new_pos)

    if N_neighbours > 0:
        # add last atom to get exact density
        r = pair_density_inverse(density_last_atom, element_pair, **kwargs)
        positions.append(np.array([0, 0, r]))
    positions = np.asarray(positions)

    # re-calculate density for debugging/comparing with desired density
    out_density = get_total_density_atom(positions, element_pair, **kwargs)
    if abs(out_density - density) > tolerance:
        print(f' WARNING: calculated density in embedding .xyz exceeds tolerance {tolerance}!!')
        print(f' density in: {density}, out: {out_density}.')

    return positions, out_density


def make_grid_points_xyz(densities, parameters, element_pair=('W', 'W'), n_xyzfiles=1):
    """ make .xyz file(s) at given densities. Returns list of xyz files (even when only one!). """

    # large enough box, but PBC is off anyway
    box_length = 30

    def write_embedding_xyz(densities, xyzname):
        """ helper function to write xyz (much faster than calling ase.io.write) """
        with open(xyzname, 'w') as xyz:
            for density in densities:
                positions, xyz_density = get_embedding_atom_positions(density, element_pair,
                                                                      parameters)
                print(len(positions), file=xyz)
                line2 = f'Lattice="{box_length} 0.0 0.0 0.0 {box_length} 0.0 0.0 0.0 {box_length}"'
                line2 += f' Properties=species:S:1:pos:R:3 pbc="F F F"'
                print(line2, file=xyz)
                for ii, pos in enumerate(positions):
                    if ii == 0:
                        print(f'{element_pair[0]} {pos[0]} {pos[1]} {pos[2]}', file=xyz)
                    else:
                        print(f'{element_pair[1]} {pos[0]} {pos[1]} {pos[2]}', file=xyz)

    # split in as many xyz files as requested
    if n_xyzfiles == 1:
        xyzname = 'embedding.xyz'
        write_embedding_xyz(densities, xyzname)
        return [xyzname]
    else:
        xyz_out = []
        i_split = np.linspace(0, len(densities), n_xyzfiles, endpoint=False, dtype=int)
        for i in range(n_xyzfiles):
            xyzname = f'embedding-{i}.xyz'
            if i == n_xyzfiles - 1:
                i_densities = densities[i_split[i]:]
            else:
                i_densities = densities[i_split[i]:i_split[i + 1]]
            write_embedding_xyz(i_densities, xyzname)
            xyz_out.append(xyzname)
        return xyz_out


def get_pair_densities(element_pair, r_interval, N, descriptor_parameters, datafile=None):
    """ Main function to be called to calculate pair density grid. Input example:
    element_pair: [A, B]
    r_interval: [0, 5]
    N: 1000
    datafile = 'AB.dat'
    """

    grid_points = np.linspace(*r_interval, N)

    kwargs = get_pair_density_kwargs(descriptor_parameters)
    pair_densities = pair_density(grid_points, element_pair, **kwargs)

    if datafile is not None:
        write_datfile(grid_points, pair_densities, datafile)

    return pair_densities


def get_energies(element_pair, density_interval, N, descriptor_parameters,
                 quip_executable, quip_potfile, e_isolated, ncores=1, datafile=None, xyzfile=None):
    """ Main function to be called to calculate energies for all grid points. Input example:
    element_pair: [A, B]
    r_interval: [0.01, 5]
    quip_executable: '/path/to/quip'
    quip_potfile: 'gap.xml'
    e_isolated = {'A': -4.5, 'B': 0.0}
    ncores = 4
    datafile = 'AB.dat'
    """

    grid_points = generate_grid_points(*density_interval, N)

    xyz_in = make_grid_points_xyz(grid_points, descriptor_parameters, element_pair, ncores)
    xyz_out = 'temp.xyz'

    energies = run_quip_xyz_grid(quip_executable, quip_potfile, xyz_in, xyz_out, e_isolated)

    if datafile is not None:
        write_datfile(grid_points, energies, datafile)

    # clean up
    if xyzfile is None:
        os.remove(xyz_out)
    else:
        os.rename(xyz_out, xyzfile)
    dir = os.getcwd()
    files = os.listdir(dir)
    for file in files:
        if file.startswith('quip.out') or file.endswith('.idx'):
            os.remove(file)
    for xyz in xyz_in:
        os.remove(xyz)

    return energies
