import numpy as np
import subprocess
from ase.data import atomic_numbers, atomic_masses
from ase.calculators.lammps import Prism
from ase.calculators.lammps import convert
from ase.constraints import voigt_6_to_full_3x3_stress
import ase.io
import os
import shutil
import uuid


def get_elements_and_masses(atoms):
    """ Get correctly sorted element list and mass list from atoms"""
    elements = list(sorted(set(atoms.get_chemical_symbols())))
    masses = [atomic_masses[atomic_numbers[element]] for element in elements]
    return elements, masses


def write_lammps_input_file(pair_style, pair_coeff, masses, pbc, tmp_id='', units='metal'):
    """ Write simple static 0-step input file for lammps (to get energy & forces of atoms)
    """

    with open(f'in.lammps{tmp_id}', 'w') as il:
        print(f'log lammps.log{tmp_id}', file=il)
        print(f'units {units}', file=il)
        pbc_string = 'boundary '
        for p in pbc:
            if p:
                pbc_string += 'p '
            else:
                pbc_string += 'f '
        print(pbc_string, file=il)
        print(f'read_data data.in{tmp_id}', file=il)
        # NOTE MASSES MUST BE IN CORRECT ORDER
        for i, mass in enumerate(masses):
            print(f'mass {i+1} {mass}', file=il)
        print(f'pair_style {pair_style}', file=il)
        for pc in pair_coeff:
            print(f'pair_coeff {pc}', file=il)
        print('compute  ep all pe/atom', file=il)
        print(f'dump mydump all custom 1 dump.lammps{tmp_id} type id x y z fx fy fz c_ep', file=il)
        print(f'thermo 1', file=il)
        print('thermo_style custom step pe pxx pyy pzz pxy pxz pyz', file=il)
        print('thermo_modify line one flush yes format 1 "THERMOLINE %8lu" format float "%20.10g"', file=il)
        print(f'fix my_fix all nve', file=il)
        print('run 0', file=il)


def run(atoms, pair_style, pair_coeff, cleanup=True, units='metal', voigt=False):
    """ Run lammps and return final potential energy, thermo data, and final atoms """

    lammps_cmd = os.environ.get('ASE_LAMMPSRUN_COMMAND')
    if lammps_cmd is None:
        raise OSError('Lammps command not set. '
                      'Set environment variable as: export ASE_LAMMPSRUN_COMMAND=/path/to/lammps_executable')

    elements, masses = get_elements_and_masses(atoms)
    pair_coeff_complete = pair_coeff.copy()
    for e in elements:
        for p in range(len(pair_coeff_complete)):
            pair_coeff_complete[p] += f' {e}'

    # unique identifier string for input files
    tmp_id = '-' + str(uuid.uuid4())

    # make run directory and go there
    out_dir = f'out-lammps{tmp_id}'
    if os.path.isdir(out_dir):
        if cleanup:
            print(f'# NOTE: run directory {out_dir} already exists, will not delete it!')
            cleanup = False
    else:
        os.makedirs(out_dir)
    os.chdir(out_dir)

    inputfile = f'in.lammps{tmp_id}'
    write_lammps_input_file(pair_style, pair_coeff_complete, masses, atoms.pbc, tmp_id=tmp_id)

    # create lammps data file
    ase.io.write(f'data.in{tmp_id}', atoms, format='lammps-data')

    # run lammps
    with open(f'lammps.stdout{tmp_id}', 'w') as lmpout:
        subprocess.call(f'{lammps_cmd} -in {inputfile}', stdout=lmpout, shell=True)

    # get thermo data
    Epot = None
    with open(f'lammps.stdout{tmp_id}', 'r') as sf:
        lines = sf.readlines()
        for line in reversed(lines):
            if 'THERMOLINE' in line:
                line = line.split()
                Epot = float(line[2])
                stress = line[3:]
                xx, yy, zz, xy, xz, yz = [-float(s) for s in stress]
                break
    if Epot is None:
        print('WARNING: LAMMPS FAIL, potential energy not found in output!')

    # read dump file (forces)
    atoms_out = ase.io.read(f'dump.lammps{tmp_id}', index=-1, format='lammps-dump-text',
                            specorder=elements)
    # ase stores energy=0 in SinglePointCalculator, replace it with Epot
    atoms_out.calc.results['energy'] = Epot

    ################### stresses (eV/Å^3) and virials (eV)
    # (from ase lammpsrun)
    # TODO does it work when lammps and ASE boxes are transformed??

    # We need to apply the Lammps rotation stuff to the stress:
    stress_tensor = np.array([[xx, xy, xz],
                              [xy, yy, yz],
                              [xz, yz, zz]])
    prism = Prism(atoms_out.get_cell())
    R = prism.rot_mat
    stress_atoms = np.dot(R, stress_tensor)
    stress_atoms = np.dot(stress_atoms, R.T)
    stress_atoms = stress_atoms[[0, 1, 2, 1, 0, 0],
				[0, 1, 2, 2, 2, 1]]
    stress = stress_atoms

    stress = convert(stress, "pressure", units, "ASE")
    if not voigt:
        stress = voigt_6_to_full_3x3_stress(stress)
    virial = -stress * atoms_out.get_volume()
    atoms_out.info['stress'] = stress
    atoms_out.info['virial'] = virial
    #############################

    # go back and clean up
    os.chdir('../')
    if cleanup:
        shutil.rmtree(out_dir)

    return atoms_out


def run_lammps_xyz(xyz_in, xyz_out, pair_style, pair_coeff):
    """ """

    if os.path.exists(xyz_out):
        raise FileExistsError(xyz_out)

    all_frames = ase.io.read(xyz_in, index=':')
    for atoms in all_frames:
        atoms_out = run(atoms, pair_style, pair_coeff)
        ase.io.write(xyz_out, atoms_out, append=True)
    return xyz_out
