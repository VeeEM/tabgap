import numpy as np
import re
import os
import subprocess
import pathlib
import xml.etree.ElementTree as ET
from tabulate.periodic_table import *

# QUIP-related stuff: functions for parsing and modifying QUIP/GAP .xml potential files and running QUIP.

class GAP:
    """ Parse and store details from GAP .xml file """

    def __init__(self, xmlfile=None):

        if xmlfile is None:
            print(f'Descriptors class needs GAP .xml-file as input!')
            exit()
        xmltree = ET.parse(xmlfile)
        xmlroot = xmltree.getroot()

        self.xmlfile = xmlfile
        self.xmlpath = pathlib.Path(xmlfile).resolve()
        self.label = xmlroot.tag
        self.e0 = [0] * 120  # initialise isolated-atom energy list, index is Z
        self.potentials = []
        self.all_descriptors = []
        self.twobody = []
        self.threebody = []
        self.eam = []
        self.nontab_descriptors = []

        # TODO support distance_Nb order 2, 3 (compact_clusters=? only)

        # get e0
        for e0 in xmlroot.iter('e0'):
            Z = int(e0.attrib['Z'])
            value = float(e0.attrib['value'])
            self.e0[Z] = value

        # get potential types (to allow sanity check)
        for pot in xmlroot.iter('Potential'):
            init_args = pot.attrib['init_args'].split()
            if init_args[0] == 'IP':
                self.potentials.append(init_args[1].lower())
            elif init_args[0].lower() == 'sum':
                # regex magic to get stuff inside {}
                for match in re.finditer(r'\{((.*?)*)\}', pot.attrib['init_args']):
                    p = match.group(1).split()[1].lower()
                    self.potentials.append(p)
        self.potentials = sorted(set(self.potentials))

        # parse descriptors and parameters
        for desc in xmlroot.iter('descriptor'):
            name = desc.text.split()[0]
            d = {'name': name, 'string': desc.text}
            split = desc.text.split('=')
            for i in range(len(split) - 1):
                param = split[i].split()[1]
                if '{' in split[i+1] or '}' in split[i+1]:
                    # e.g. species_Z = {74 23}
                    value = split[i+1].split('}')[0]
                    value = value.replace('{', '')
                    value = value.split()
                else:
                    value = split[i+1].split()[0]
                d[param] = value
            self.all_descriptors.append(d)
            if name == 'distance_2b':
                self.twobody.append(d)
            elif name == 'angle_3b':
                self.threebody.append(d)
            elif name == 'eam_density':
                self.eam.append(d)
            else:
                self.nontab_descriptors.append(d)

        # set max cutoffs
        self.rcutmax_2b = None
        self.rcutmax_3b = None
        self.rcutmax_eam = None
        if len(self.twobody) > 0:
            self.rcutmax_2b = max([float(d['cutoff']) for d in self.twobody])
        if len(self.threebody) > 0:
            self.rcutmax_3b = max([float(d['cutoff']) for d in self.threebody])
        if len(self.eam) > 0:
            self.rcutmax_eam = max([float(d['cutoff']) for d in self.eam])

        # get elements
        Zs = []
        for d in self.all_descriptors:
            if 'Z' in d.keys():
                Zs.append(int(d['Z']))
            if 'Z1' in d.keys():
                Zs.append(int(d['Z1']))
            if 'Z2' in d.keys():
                Zs.append(int(d['Z2']))
            if 'species_Z' in d.keys():
                Zs.extend(list(map(int, d['species_Z'])))
        Zs = sorted(set(Zs))
        self.atomic_numbers = Zs
        self.elements = [element_symbols[Z] for Z in Zs]


    def get_2b_descriptors(self, elements=None):
        """ elements: list of elements, e.g. ['W', 'V'] """
        if elements is None:
            return self.twobody
        else:
            if not isinstance(elements, list):
                raise ValueError('elements must be list of chemical symbols!')
            Zs = [atomic_numbers[e] for e in elements]
            out = []
            for d in self.twobody:
                Z1 = int(d['Z1'])
                Z2 = int(d['Z2'])
                if Z1 in Zs or Z2 in Zs:
                    out.append(d)
            return out


    def get_3b_descriptors(self, elements=None):
        """ elements: list of elements, e.g. ['W', 'V'] """
        if elements is None:
            return self.threebody
        else:
            if not isinstance(elements, list):
                raise ValueError('elements must be list of chemical symbols!')
            Zs = [atomic_numbers[e] for e in elements]
            out = []
            for d in self.threebody:
                Z = int(d['Z'])
                Z1 = int(d['Z1'])
                Z2 = int(d['Z2'])
                if Z1 in Zs or Z2 in Zs or Z in Zs:
                    out.append(d)
            return out


    def get_eam_descriptors(self, elements=None):
        """ elements: list of elements, e.g. ['W', 'V'] """
        if elements is None:
            return self.eam
        else:
            if not isinstance(elements, list):
                raise ValueError('elements must be list of chemical symbols!')
            Zs = [atomic_numbers[e] for e in elements]
            out = []
            for d in self.eam:
                Z = int(d['Z'])
                if Z in Zs:
                    out.append(d)
                # species_Z = list(map(int, d['species_Z']))
                # if Z in Zs or any(i in species_Z for i in elements):
            return out


def make_zeroed_xml(xmlfile_in, xmlfile_out, zero_2b=False, zero_3b=False, zero_eam=False,
                    keep_these_eams=None):
    """ Make GAP xml file with zeroed out 2b+Glue/3b/eam contributions.
    To get 2b energies: zero out eam (3b is invisible)
    To get 3b energies: zero out eam and 2b (including Glue pairpot)
    To get embedding energies: zero out 2b and 3b (including Glue pairpot)
    To get specific embedding energies in case of multiple descriptors for same element:
        zero=True for all 2b, 3b, and eam BUT give list of descriptor strings
        (GAP.eam[i]['string']) that should be kept as keep_these_eams=[..]
    """

    gap = GAP(xmlfile_in)

    # sanity-checks
    if len(gap.nontab_descriptors) > 0:
        print(f'WARNING: {xmlfile_in} contains unsupported descriptor(s): {gap.nontab_descriptors}')
        print(f'Tabulation will not be 1-to-1 GAP --> tabGAP!')
    supported_pots = ['glue', 'gap']
    for pot in gap.potentials:
        if pot not in supported_pots:
            print(f'WARNING: {xmlfile_in} contains unsupported external (core) potential type: {pot}')
            print(f'Tabulation will not be 1-to-1 GAP --> tabGAP!')
    if 'gap' not in gap.potentials:
        print(f'WARNING: No GAPs in {xmlfile_in}, nothing to zero out! Continuing anyway..')
    if keep_these_eams is not None and not isinstance(keep_these_eams, list):
        raise ValueError('keep_these_eams must be list of descriptor strings')

    trigger_zero_glue = False
    trigger_zero_gap = False

    # copy xml file line by line and edit certain lines
    with open(xmlfile_in, 'r') as xfin:
        with open(xmlfile_out, 'w') as xfout:
            for i, line in enumerate(xfin):
                line = line.rstrip()

                if zero_2b:
                    ##### Glue pair potential
                    # NOTE!!! here assumes embedding is already zero (otherwise why tabulate..)
                    if '<potential_pair' in line and 'glue' in gap.potentials:
                        trigger_zero_glue = True

                    if trigger_zero_glue:
                        if 'point r=' not in line:
                            if '</potential_pair' in line:
                                trigger_zero_glue = False  # reached end of pair potential
                            elif '<potential_pair' not in line:
                                raise Exception(f'tried to zero out wrong Glue line: {line}')
                        else:
                            splitline = line.split('"')
                            splitline[3] = '0.0'  # zero energy
                            line = '"'.join(splitline)

                ###### GAP
                # TODO NOTE distance_Nb not tested!!
                if ('distance_2b' in line and 'descriptor' in line or 'distance_Nb' in line and
                        'order=2' in line and 'descriptor' in line):
                    if zero_2b:
                        trigger_zero_gap = True
                elif ('angle_3b' in line and 'descriptor' in line or 'distance_Nb' in line and
                        'order=3' in line and 'descriptor' in line):
                    if zero_3b:
                        trigger_zero_gap = True
                elif 'eam_density' in line and 'descriptor' in line:
                    if zero_eam:
                        if keep_these_eams is None:
                            trigger_zero_gap = True
                        else:
                            if not any(eam_string in line for eam_string in keep_these_eams):
                                trigger_zero_gap = True

                if trigger_zero_gap:
                    if 'sparseX i=' not in line:
                        if '</gpCoordinates' in line:
                            trigger_zero_gap = False  # reached end of descriptor alphas
                        elif '<descriptor>' not in line and '<permutation' not in line:
                            raise Exception(f'tried to zero out wrong GAP line: {line}')
                    else:
                        splitline = line.split('"')
                        splitline[3] = '0.0'  # zero alpha
                        line = '"'.join(splitline)

                print(line, file=xfout)


def run_quip_xyz_grid(quip, potfile, xyz_in, xyz_out, e_isolated):
    """ run quip jobs on xyz_in and return energies. If xyz_in is list of files, do all of them in parallel """

    # append 'atoms_filename=xyzfile' later
    quip_args = [quip, 'E=T', 'F=T', 'local=T', f'param_filename={potfile}']

    if not isinstance(xyz_in, list):
        xyz_in = [xyz_in]
    ncores = len(xyz_in)

    # trivially parallelise
    if ncores > 1:
        # launch ncores quip jobs and wait for them to finish
        jobs = []
        for i in range(ncores):
            # print(f'  started parallel run {i}')
            qout = open(f'quip.out-{i}', 'w')  # TODO is this ever closed?
            run_command = quip_args.copy()
            run_command.append(f'atoms_filename={xyz_in[i]}')
            jobs.append(subprocess.Popen(run_command, stdout=qout))
        for job in jobs:
            job.wait()

        # grep xyz lines (fast, no need for parallel runs...)
        for i in range(ncores):
            run_grep = f'''grep "AT" quip.out-{i} | sed 's/AT//' '''
            if i == 0:
                # open new xyz_out
                with open(xyz_out, 'w') as gout:
                    subprocess.call(run_grep, stdout=gout, shell=True)
            else:
                # append to xyz_out
                with open(xyz_out, 'a') as gout:
                    subprocess.call(run_grep, stdout=gout, shell=True)

    else:
        # one core TODO update to Popen as above?
        quip_args = ' E=T F=T local=T atoms_filename={} param_filename={}'.format(xyz_in[0], potfile)
        run_quip = quip + quip_args
        run_grep = '''grep "AT" quip.out | sed 's/AT//' '''
        # print(f' running quip for {xyz_in}')
        with open('quip.out', 'w') as qout:
            subprocess.call(run_quip, stdout=qout, shell=True)
        # grep-sed xyz data
        with open(xyz_out, 'w') as gout:
            subprocess.call(run_grep, stdout=gout, shell=True)

    # parse energies (ugly...)
    # NOTE! assumes:
    # 1. same number of atoms in all frames
    # 2. same order of atoms, with first atom the central atom "i"
    # 3. xyz file is straight from quip
    # This is true for 2b and 3n energies, but not eam. This is much faster than using ase.io.read
    # For eam, let fail and do slower manual loop.
    xyz = open(xyz_out, 'r')
    lines = xyz.readlines()  # NOTE load all lines into memory (should be fine)

    if len(lines) == 0:
        raise RuntimeError(f'no lines in {xyz_out}, quip run probably crashed')

    do_slow_xyz_read = False
    natoms = int(lines[0])  # try assuming same natoms in all frames
    atom_i_lines = lines[2::natoms + 2]
    e0_i = e_isolated[int(atom_i_lines[0].split()[4])]  # Z in col 5 of quip xyz

    if natoms == 3:
        # return energy of i for triplet ijk
        try:
            energies = [float(line.split()[-1]) - e0_i for line in atom_i_lines]
        except ValueError:
            do_slow_xyz_read = True
        energies = np.asarray(energies)
    elif natoms == 2:
        # return bond energy of dimer ij
        atom_j_lines = lines[3::natoms + 2]
        e0_j = e_isolated[int(atom_j_lines[0].split()[4])]
        try:
            energies_i = [float(line.split()[-1]) - e0_i for line in atom_i_lines]
            energies_j = [float(line.split()[-1]) - e0_j for line in atom_j_lines]
        except ValueError:
            do_slow_xyz_read = True
        energies = np.sum([energies_i, energies_j], axis=0)
    else:
        do_slow_xyz_read = True

    if do_slow_xyz_read:
        # get energy of atom i  (first atom)
        energies = []
        ii = 0
        while ii < len(lines):
            natoms = int(lines[ii])
            ii += 2
            atom_i_line = lines[ii]
            energies.append(float(atom_i_line.split()[-1]) - e0_i)
            ii += natoms
        energies = np.asarray(energies)

    xyz.close()
    return energies


def run_quip_xyz(quip, potfile, xyz_in, xyz_out):
    """ run quip on xyz_in and return output xyz """

    # append 'atoms_filename=xyzfile' later
    quip_args = [quip, 'E=T', 'F=T', 'local=T', f'param_filename={potfile}']

    quip_args = f' E=T F=T local=T atoms_filename={xyz_in} param_filename={potfile}'
    run_quip = quip + quip_args
    run_grep = '''grep "AT" quip.out | sed 's/AT//' '''
    # print(f' running quip for {xyz_in}')
    with open('quip.out', 'w') as qout:
        subprocess.call(run_quip, stdout=qout, shell=True)
    # grep-sed xyz data
    with open(xyz_out, 'w') as gout:
        subprocess.call(run_grep, stdout=gout, shell=True)

    # clean
    dir = os.getcwd()
    files = os.listdir(dir)
    for file in files:
        if file.startswith('quip.out') or file.endswith('.idx'):
            os.remove(file)

    return xyz_out
