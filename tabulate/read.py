import numpy as np
from tabulate.periodic_table import *


def read_tabgap_potfile(filename):
    """ Read .tabgap potential file written by write.write_tabgap_potfile
        Returns a dict with all inputs of write function: elements, e0, energies_2b,
        energies_3b, r2b, r3b, n2b, n3b, line1, line2.
    """

    energies_2b = {}
    energies_3b = {}
    r2b, n2b, r3b, n3b = None, None, None, None

    with open(filename, 'r') as pf:

        # 2 first lines are comments
        line1 = next(pf).strip()
        line2 = next(pf).strip()

        # 3rd line is isolated atom energies e0
        e0_line = next(pf).split()
        N_elements = int(e0_line[0])
        elements = []
        e0 = {}
        for i in range(N_elements):
            elements.append(e0_line[2*i+1])
            e0[atomic_numbers[e0_line[2*i+1]]] = float(e0_line[2*i+2])

        # 4th line is number of 2b and 3b pots
        N_2bpots, N_3bpots = map(int, next(pf).split())

        # then comes the data, one pot at a time with a header line
        # 2b
        for i in range(N_2bpots):
            # ele1 ele2 rmin rcut npoints
            header = next(pf).split()
            pair = (header[0], header[1])
            energies_2b[pair] = []
            # NOTE these will update every iteration, but current read function assumes
            # they are same for all pairs
            r2b = (float(header[2]), float(header[3]))
            n2b = int(header[4])
            for j in range(n2b+2):
                energies_2b[pair].append(float(next(pf)))

        # 3b
        for i in range(N_3bpots):
            # header columns: ele1 ele2 ele3 rij_min rik_min costheta_min rij_max rik_max
            # costheta_max nij nik ncostheta
            header = next(pf).split()
            triplet = (header[0], header[1], header[2])
            energies_3b[triplet] = []
            r3b = (float(header[3]), float(header[6]))
            n3b = (int(header[9]), int(header[10]), int(header[11]))
            Ndata = (n3b[0]+2) * (n3b[1]+2) * (n3b[2]+2)
            for j in range(Ndata):
                energies_3b[triplet].append(float(next(pf)))

    R = {'elements': elements,
         'e0': e0,
         'energies_2b': energies_2b,
         'energies_3b': energies_3b,
         'r2b': r2b, 'r3b': r3b,
         'n2b': n2b, 'n3b': n3b,
         'line1': line1, 'line2': line2,
         'distances_2b': None,
        }
    # save 2b distances, to be consistent with read-in eam data.
    # NOTE n2b+2 due to spline filtering, will be small mismatch in r vs. E
    if r2b is not None and n2b is not None:
        R['distances_2b'] = np.linspace(r2b[0], r2b[1], n2b+2)

    return R


def read_eam_potfile(filename, style='eam/fs'):
    """ Read embedding energy, pair density, and pair potential
        from eam/fs lammps potential file """

    supported_styles = ['eam/fs', 'eam/alloy']
    if style not in supported_styles:
        raise NotImplementedError(f' eam pair_style {style} for file {filename} not supported.')

    energies_2b = {}
    energies_eam = {}
    pair_densities = {}
    lattices = {}
    lattice_constants = {}

    with open(filename, 'r') as pf:

        # first 3 lines are comments
        line1 = next(pf).strip()
        line2 = next(pf).strip()
        line3 = next(pf).strip()

        # 4th line lists elements and how many, e.g. 2 W Ta
        elements = list(next(pf).split()[1:])

        # 5th line contains grid details
        N_rho, d_density, N_r, d_r, rcut = list(map(float, next(pf).split()))
        N_rho = int(round(N_rho))
        N_r = int(round(N_r))
        rho = np.linspace(0, d_density*(N_rho-1), N_rho)

        # then embedding energy and pair density functions for each element and
        # pair, with a header line before each new embedding element
        for i in range(len(elements)):
            # header file: Z mass a lattice. Only need Z.
            cols = next(pf).split()
            Z = int(cols[0])
            s = element_symbols[Z]
            lattices[s], lattice_constants[s] = cols[3], cols[2]
            # sanity-check
            if s not in elements:
                raise LookupError(f'Parsed embedding element {s} not found in elements'
                                  f' listed on 4th line: {elements}')
            # embedding energies, support multiple columns (but not varying ncols...)
            first_line = list(map(float, next(pf).strip().split()))
            ncols = len(first_line)
            energies_eam[s] = first_line
            for j in range(ncols, N_rho, ncols):
                energies_eam[s].extend(list(map(float, next(pf).strip().split())))
            assert(len(energies_eam[s]) == N_rho)  # sanity-check

            if style == 'eam/fs':
                # pair densities in correct pair order 1-s, 2-s, ..., N-s.
                # note that there are data for both ij and ji pairs
                for j, s2 in enumerate(elements):
                    # pair = (s2, s)
                    pair = s2 + s
                    # support multiple columns
                    first_line = list(map(float, next(pf).strip().split()))
                    ncols = len(first_line)
                    pair_densities[pair] = first_line
                    for k in range(ncols, N_r, ncols):
                        pair_densities[pair].extend(list(map(float, next(pf).strip().split())))
                    assert(len(pair_densities[pair]) == N_r)  # sanity-check
            if style == 'eam/alloy':
                # only one density function per element rho = rho_j
                # first get data for element, then copy to all pairs to be consistent with eam/fs
                first_line = list(map(float, next(pf).strip().split()))
                ncols = len(first_line)
                pp = first_line
                for k in range(ncols, N_r, ncols):
                    pp.extend(list(map(float, next(pf).strip().split())))
                assert(len(pp) == N_r)  # sanity-check
                for j, s2 in enumerate(elements):
                    # pair = (s2, s)
                    pair = s2 + s
                    pair_densities[pair] = pp

        # pair potentials in correct order, 1-1, 2-1, 2-2, 3-1, 3-2, 3-3, ...
        # data is r*pairpot, so need to divide by r to get energy
        r = np.linspace(0, rcut, N_r)
        r[0] = 1e-6  # avoid division by zero, corrected afterwards
        for i, s in enumerate(elements):
            for j in range(0, i+1):
                s2 = elements[j]
                pair = (s2, s)
                # support multiple columns
                first_line = list(map(float, next(pf).strip().split()))
                ncols = len(first_line)
                energies_2b[pair] = [rE / r[k] for k, rE in enumerate(first_line)]
                for k in range(ncols, N_r, ncols):
                    line = list(map(float, next(pf).strip().split()))
                    energies_2b[pair].extend([rE / r[k+l] for l, rE in enumerate(line)])
                # first point is 0 because read-in data is r*E, make it same
                # as 2nd point to (maybe) avoid confusion when plotting...
                energies_2b[pair][0] = energies_2b[pair][1]
                assert(len(energies_2b[pair]) == N_r)  # sanity-check
        r[0] = 0  # division by zero avoided, correct

    R = {'elements': elements,
         'energies_2b': energies_2b,
         'distances_2b': r,
         'energies_eam': energies_eam,
         'pair_densities': pair_densities,
         'density': rho,
         'rcut': rcut,
         'd_r': d_r,
         'd_density': d_density,
         'lattices': lattices,
         'lattice_constants': lattice_constants,
         'N_r': N_r,
         'N_rho': N_rho,
         'comment_lines': [line1, line2, line3]
        }
    return R
