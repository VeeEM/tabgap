import os
import numpy as np
from tabulate.quip_tools import *
from tabulate.spline_tools import filter_coeffs
from tabulate.write import *

# three-body tabulation tools


def generate_grid_points(rmin, rcut, n, repmin=0):
    """ Generate points in the (rij, rik, costheta) grid and get points that needs energy calculation.
        Number of points is given by n as (n_rij, n_rik, n_costheta).
        Permutation invariance of j-k is used to reduce energy calculations.
        If repmin is given, no energy calc is initialised for points where rij or ik < repmin
        (because when using 3b reppot cutoff they have zero energy).
        Returns grid (where e.g. grid[:, :, :, 0] = r_ij) and indices for energy call as list of (i, j, k) tuples.
    """
    rij = np.linspace(rmin, rcut, n[0])
    rik = np.linspace(rmin, rcut, n[1])
    # NOTE hard-coded from -1 to 1 (180 to 0 deg)
    costheta = np.linspace(-1, 1, n[2])
    grid = np.zeros((n[0], n[1], n[2], 3))
    ijk_calc = []
    # could probably vectorise this somehow with np.meshgrid
    for i in range(n[0]):
        for j in range(n[1]):
            for k in range(n[2]):
                grid[i, j, k, 0] = rij[i]
                grid[i, j, k, 1] = rik[j]
                grid[i, j, k, 2] = costheta[k]
                if rij[i] > repmin and rik[j] > repmin and j >= i:
                    ijk_calc.append((i, j, k))
    # print(f'  {np.prod(n)} grid points, {len(ijk_calc)} needed for energy calculation')

    # # vectorising testing
    # rij, rik, costheta = np.meshgrid(np.linspace(rmin, rcut, n[0]),
                                     # np.linspace(rmin, rcut, n[1]),
                                     # np.linspace(-1, 1, n[2]),
                                     # indexing='ij', sparse=False, copy=True)
    # grid = np.array([rij, rik, costheta])
    # grid = np.meshgrid(np.linspace(rmin, rcut, n[0]),
                                     # np.linspace(rmin, rcut, n[1]),
                                     # np.linspace(-1, 1, n[2]),
                                     # indexing='ij', sparse=False, copy=True)
    # grid = np.reshape(grid, (3, np.prod(n)))

    return grid, ijk_calc


def get_element_triplets(elements, jk_symmetry=True):
    """ get all element triplets, with ijk=ikj symmetry or not """
    triplets = []
    for i, s1 in enumerate(elements):
        for j, s2 in enumerate(elements):
            for k, s3 in enumerate(elements):
                if not jk_symmetry:
                    triplets.append((s1, s2, s3))
                else:
                    if k >= j:
                        triplets.append((s1, s2, s3))
    return triplets


def fill_in_triplet_energies(energies, grid, n, ijk_calc, repmin, repmax=None):
    """ exploit jk permutation and repmin to fill in missing energies. Also apply 3b reppot cutoff if desired. """

    assert(len(energies) == len(ijk_calc))

    def inner_cutoff(r, r1, r2):
        """ smooth 'Perriot' polynomial reppot cutoff function (inverted compared to normal cutoff!) """
        if r < r1:
            return 0
        elif r > r2:
            return 1
        else:
            chi = (r - r1) / (r2 - r1)
            return chi**3 * (6.0*chi**2 - 15*chi + 10.0)

    energy_grid = np.zeros(n)
    new_energies = []
    for i, ijk in enumerate(ijk_calc):
        energy_grid[ijk] = energies[i]

    for i in range(n[0]):
        for j in range(n[1]):
            for k in range(n[2]):
                if grid[i, j, k, 0] < repmin or grid[i, j, k, 1] < repmin:
                    energy_grid[i, j, k] = 0
                elif j < i:
                    # note: looks misleading here with ij=ji (and not jk), but that means swapping rij<->rik
                    energy_grid[i, j, k] = energy_grid[j, i, k]
                if repmax is not None:
                    rij = grid[j, i, k, 0]
                    rik = grid[j, i, k, 1]
                    fc = inner_cutoff(rij, repmin, repmax) * inner_cutoff(rik, repmin, repmax)
                    energy_grid[i, j, k] *= fc
                new_energies.append(energy_grid[i, j, k])
    return np.asarray(new_energies)


def make_grid_points_xyz(triplets, triplet_symbols=('W', 'W', 'W'), n_xyzfiles=1,
                       overlap_tolerance=1e-6):
    """ make .xyz file(s) with isolated triplets.
        Note: in costheta grid, when j and k atoms overlap within overlap_tolerance (when rij = rik and theta=0),
        the k atom is shifted in x by overlap_tolerance to avoid QUIP 'atoms overlap exactly' error.
        Returns list of xyz files (even when only one!).
    """

    # make sure box_length > 3*rcut (even though PBC is off....?)
    box_length = 30
    rmax = max((max(triplets[:, 0]), max(triplets[:, 1])))
    if 3 * rmax > box_length:
        box_length = int(3 * rmax + 5)

    def write_triplet_xyz(triplets, xyzname):
        """ helper function to write xyz (much faster than calling ase.io.write) """
        with open(xyzname, 'w') as xyz:
            for triplet in triplets:
                rij, rik, costheta = triplet
                xk = rik * costheta
                yk = rik * np.sin(np.arccos(costheta))
                # check if j k overlap
                rjk = np.linalg.norm((rij - xk, 0 - yk))
                if rjk < overlap_tolerance:
                    # NOTE here brutally makes them not overlap exactly
                    xk += overlap_tolerance
                positions=[(0, 0, 0), (rij, 0, 0), (xk, yk, 0)]
                print(len(positions), file=xyz)
                line2 = f'Lattice="{box_length} 0.0 0.0 0.0 {box_length} 0.0 0.0 0.0 {box_length}"'
                line2 += f' Properties=species:S:1:pos:R:3 pbc="F F F"'
                print(line2, file=xyz)
                for ii, pos in enumerate(positions):
                    print(f'{triplet_symbols[ii]} {pos[0]} {pos[1]} {pos[2]}', file=xyz)

    # split in as many xyz files as requested
    if n_xyzfiles == 1:
        xyzname = 'triplets.xyz'
        write_triplet_xyz(triplets, xyzname)
        return [xyzname]
    else:
        xyz_out = []
        i_split = np.linspace(0, len(triplets), n_xyzfiles, endpoint=False, dtype=int)
        for i in range(n_xyzfiles):
            xyzname = f'triplets-{i}.xyz'
            if i == n_xyzfiles - 1:
                i_triplets = triplets[i_split[i]:]
            else:
                i_triplets = triplets[i_split[i]:i_split[i + 1]]
            write_triplet_xyz(i_triplets, xyzname)
            xyz_out.append(xyzname)
        return xyz_out


def get_energies(elements, r_interval, N, quip_executable, quip_potfile, e_isolated,
                 ncores=1, datafile=None, xyzfile=None, reppot_interval=[0, None]):
    """ Main function to be called to calculate energies for all grid points. Input example:
    elements: [A, B, C]
    r_interval: [0.01, 5]
    N: [40, 40, 40]
    quip_executable: '/path/to/quip'
    quip_potfile: 'gap.xml'
    e_isolated = {'A': -4.5, 'B': 0.0, 'C': -2.5}
    ncores = 4
    datafile = 'ABC.dat'
    """

    # TODO option to create zeroed-out potfile, option to keep xyz_out

    grid_points, ijk_calc = generate_grid_points(*r_interval, N, reppot_interval[0])
    grid_points_calc = np.array([grid_points[ijk] for ijk in ijk_calc])

    xyz_in = make_grid_points_xyz(grid_points_calc, elements, ncores)
    xyz_out = 'temp.xyz'

    energies = run_quip_xyz_grid(quip_executable, quip_potfile, xyz_in, xyz_out, e_isolated)
    energies = fill_in_triplet_energies(energies, grid_points, N, ijk_calc, *reppot_interval)

    if datafile is not None:
        write_datfile(grid_points, energies, datafile)

    energies = filter_coeffs(*r_interval, N, energies)  # required spline filtering

    # clean up
    if xyzfile is None:
        os.remove(xyz_out)
    else:
        os.rename(xyz_out, xyzfile)
    dir = os.getcwd()
    files = os.listdir(dir)
    for file in files:
        if file.startswith('quip.out') or file.endswith('.idx'):
            os.remove(file)
    for xyz in xyz_in:
        os.remove(xyz)

    return np.ndarray.flatten(energies)
