import ase.io
import shutil
import os
import numpy as np
from tabulate.quip_tools import run_quip_xyz
from tabulate.lammps import run_lammps_xyz
import tabulate.eam


def get_rmse_from_xyz(xyz_1, xyz_2):
    """ calculate RMS errors from multi-frame gap_fit/QUIP-formatted extxyz files """

    atoms_1 = ase.io.read(xyz_1, ':')
    atoms_2 = ase.io.read(xyz_2, ':')
    E_1 = [atoms.get_potential_energy() / len(atoms) for atoms in atoms_1]
    E_2 = [atoms.get_potential_energy() / len(atoms) for atoms in atoms_2]
    E_1 = np.asarray(E_1)
    E_2 = np.asarray(E_2)

    error2 = (E_2 - E_1)**2
    E_rms = np.sqrt(np.mean(error2)) * 1e3  # meV
    # E_std = np.sqrt(np.var(error2)) * 1e3

    # NOTE all atoms/symbols combined
    try:
        F_1 = [atoms.arrays['force'][i] for atoms in atoms_1 for i in range(len(atoms))]
    except KeyError:
        F_1 = [atoms.arrays['forces'][i] for atoms in atoms_1 for i in range(len(atoms))]
    try:
        F_2 = [atoms.arrays['force'][i] for atoms in atoms_2 for i in range(len(atoms))]
    except KeyError:
        F_2 = [atoms.arrays['forces'][i] for atoms in atoms_2 for i in range(len(atoms))]
    F_1 = np.asarray(F_1)
    F_2 = np.asarray(F_2)

    error2 = (F_2 - F_1)**2
    F_rms = np.sqrt(np.mean(error2))
    # F_std = np.sqrt(np.var(error2))

    # check if we find virials
    s_rms = None
    # s_std = None
    do_virials = True
    for atoms in atoms_1:
        if 'virial' not in atoms.info:
            do_virials = False
            break
    for atoms in atoms_2:
        if 'virial' not in atoms.info:
            do_virials = False
            break
    if do_virials:
        s_1 = [atoms.info['virial'] for atoms in atoms_1]
        s_2 = [atoms.info['virial'] for atoms in atoms_2]
        s_1 = np.asarray(s_1)
        s_2 = np.asarray(s_2)
        error2 = (s_2 - s_1)**2
        s_rms = np.sqrt(np.mean(error2))
        # s_std = np.sqrt(np.var(error2))

    return {'energy': E_rms, 'force': F_rms, 'virial': s_rms}


def get_interpolation_error(xyzfile, pair_style, pair_coeff_without_elements,
                            compute_gap=False, gap_xml=None, save_xyz=False):
    """ compute RMSE between tabGAP and GAP for given xyz file.
    pair_coeff_without_elements should be list of pair_coeff lines, without the
    atomic numbers at the end of each line.
    Preferably, the xyz file already contains GAP energies & forces, otherwise
    they are also computed """

    temp1_xyz = 'intp_rmse_ref.xyz'
    temp2_xyz = 'intp_rmse_tabgap.xyz'

    all_frames = ase.io.read(xyzfile, index=':')
    if not compute_gap:
        # test if we find energy
        try:
            all_frames[0].get_potential_energy()
        except:
            print(f'NOTE: GAP energy not found in input xyz {xyzfile}, computing them..')
            compute_gap = True
            if gap_xml is None:
                raise ValueError('...no GAP xml file, give path as gap_xml=/path/to/gap.xml')

    if compute_gap:
        quip_command = os.environ.get('QUIP_COMMAND')
        if quip_command is None:
            raise OSError('Environment variable QUIP_COMMAND not set. '
                          'Do: export QUIP_COMMAND=/path/to/quip_executable')
        xyz_ref = run_quip_xyz(quip_command, str(gap_xml), xyzfile, temp1_xyz)
    else:
        shutil.copyfile(xyzfile, temp1_xyz)

    # tabgap
    run_lammps_xyz(xyzfile, temp2_xyz, pair_style, pair_coeff_without_elements)

    rmse_dict = get_rmse_from_xyz(temp2_xyz, temp1_xyz)

    if not save_xyz:
        os.remove(temp1_xyz)
        os.remove(temp2_xyz)

    return rmse_dict


def compute_eam_densities_atoms(atoms, GAP):
    """ Get total eam_density for every atom in ASE atoms object.
    Supports multiple eam_density-descriptors per element.
    GAP is GAP class object.
    Output is list of list of densities, one list per descriptor per element. """

    # TODO optimise (not sure if neighborlist worth it, given that this should be
    # used with DFT-sized training-data boxes.)

    # get indices of each element and descriptors
    elements = list(set(atoms.get_chemical_symbols()))
    element_indices = {}
    element_descriptors = {}
    for e in elements:
        element_indices[e] = [i for i, s in enumerate(atoms.get_chemical_symbols()) if e==s]
        element_descriptors[e] = GAP.get_eam_descriptors([e])
    n_eams = max([len(d) for d in element_descriptors.values()])

    # get total eam density of each atom in all eam terms
    rho = []
    for n in range(n_eams):
        rho.append([])
        for i, atom in enumerate(atoms):
            rho_tot = 0
            try:
                desc = element_descriptors[atom.symbol][n]
            except IndexError:
                # reached max number of embedding terms for this element
                rho[n].append(rho_tot)
                continue
            # do each element separately, slow but easiest
            kwargs = tabulate.eam.get_pair_density_kwargs(desc)
            for e2 in elements:
                indices = element_indices[e2].copy()
                if i in indices: indices.remove(i)
                rij = atoms.get_distances(i, indices, mic=True)
                phi_ij = tabulate.eam.pair_density(rij, (atom.symbol, e2), **kwargs)
                rho_tot += np.sum(phi_ij)
            rho[n].append(rho_tot)
        # TODO add rho to atoms and option to save xyz?

    return rho


def get_rho_max_from_xyz(xyzfile, GAP, verbose=False):
    """ """

    all_frames = ase.io.read(xyzfile, index=':')
    rho_max, i_max = 0, None
    for i, atoms in enumerate(all_frames):
        rho = compute_eam_densities_atoms(atoms, GAP)
        rho_trial = np.amax(rho)
        if rho_trial > rho_max:
            rho_max = rho_trial
            i_max = i
        if verbose:
            print(f' get_rho_max: frame {i}/{len(all_frames)}, rho_max now: {rho_max}')
    return rho_max
