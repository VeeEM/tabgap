import numpy as np
from tabulate.periodic_table import *


def write_tabgap_potfile(filename, elements, e0, energies_2b, energies_3b, r2b, r3b, n2b, n3b,
                         line1='This line is a comment', line2='This line is also a comment'):
    """ Write potential file for lammps pair_style tabgap.
        First two line are comments, add some useful details there with line1, line2 arguments.
        3rd line is number of elements and energies of isolated atoms, e.g. 1 W -4.57.
        4th line is number of 2b pots and 3b pots, e.g. 3 6.
        5th line+ data (energies) with header for each potential (see below).
    """

    if energies_2b is None:
        energies_2b = {}
    if energies_3b is None:
        energies_3b = {}

    with open(filename, 'w') as pf:

        # 2 first lines are comments
        print(line1, file=pf)
        print(line2, file=pf)

        # 3rd line is isolated atom energies e0
        e0_line = f'{len(elements)}'
        for element in elements:
            e0_line += f' {element} {e0[atomic_numbers[element]]}'
        print(e0_line, file=pf)

        # 4th line is number of 2b and 3b pots
        print(f'{len(energies_2b.keys())} {len(energies_3b.keys())}', file=pf)

        # then comes the data, one pot at a time with header
        # 2b
        for pair, data in energies_2b.items():
            # ele1 ele2 rmin rcut npoints
            header = f'{pair[0]} {pair[1]} {r2b[0]} {r2b[1]} {n2b}'
            print(header, file=pf)
            for e in data:
                print(e, file=pf)

        # 3b
        for triplet, data in energies_3b.items():
            # ele1 ele2 ele3 rij_min rik_min costheta_min rij_max rik_max costheta_max nij nik ncostheta
            header = f'{triplet[0]} {triplet[1]} {triplet[2]} {r3b[0]} {r3b[0]} -1.0 {r3b[1]} {r3b[1]} 1.0'
            header += f' {n3b[0]} {n3b[1]} {n3b[2]}'
            print(header, file=pf)
            for e in data:
                print(e, file=pf)


def write_eam_fs_potfile(filename, elements, r, pair_energies, pair_densities, embedding_energies,
                         d_r, d_density, rcut, lattice_constants, lattices, comment_lines=['comment'] * 3):
    """ https://lammps.sandia.gov/doc/pair_eam.html
    pair_style eam/fs
    See above link for format. Supports many elements.
    Pair potential interactions are symmetric ij = ji, but pair density contributions may not be, i.e. ij != ji  !!!
    Note that r, d_r, d_density, and rcut must be the same for all element pairs!
    elements should be list/array of elements, e.g. ['A', 'B'], assuming cross-interactions between all.
    pair_energies, pair_densities, embedding_energies, lattice_constants, and lattices should all be
    dictionaries with key as the element pair (e.g. 'AB') or element ('A'). All elements/pairs must exist.
    Pair energies, densities, embedding energies must all be provided in the same interval!!!
    """

    # some sanity-checks in input
    N_rho = len(list(embedding_energies.values())[0])
    for data in embedding_energies.values():
        assert(N_rho == len(data))
    N_r = len(r)
    for data in pair_energies.values():
        assert(N_r == len(data))
    for data in pair_densities.values():
        assert(N_r == len(data))

    with open(filename, 'w') as pf:

        print(comment_lines[0], file=pf)
        print(comment_lines[1], file=pf)
        print(comment_lines[2], file=pf)

        elements_line = f'{len(elements)}'
        for e in elements:
            elements_line += f'  {e}'
        print(elements_line, file=pf)

        print(f'{N_rho} {d_density} {N_r} {d_r} {rcut}', file=pf)

        #### First embedding and pair density functions for each element and element pair
        for i, e in enumerate(elements):
            # header
            Z = atomic_numbers[e]
            mass = atomic_masses[Z]
            print(f'{Z} {mass} {lattice_constants[e]} {lattices[e]}', file=pf)
            for F in embedding_energies[e]:
                print(F, file=pf)

            # loop over pairs in correct lammps order: 1e, 2e, ..., Ne, where e is current element
            for j, e2 in enumerate(elements):
                # TODO make sure this is correct, because lammps seems to read in them as e+e2 !???
                pair = e2 + e
                for d in pair_densities[pair]:
                    print(d, file=pf)

        #### Then pair potentials in correct order: (1, 1), (2, 1), (2, 2), (3, 1), (3, 2), (3, 3), ...
        # no header, and lammps wants data as r * pairpot
        for i, e in enumerate(elements):
            for j in range(0, i+1):
                # allow symmetry
                try:
                    data = pair_energies[(e, elements[j])]
                except KeyError:
                    data = pair_energies[(elements[j], e)]
                for k, energy in enumerate(data):
                    print(r[k] * energy, file=pf)


def write_datfile(grid, energies, filename):
    """ save file with energies for every grid point (r_pair, eam_pair_density or triplet) """

    # TODO make arrays and use numpy's write functions
    #       - can they append to file (need headers..) ?

    with open(filename, 'w') as pf:
        if len(np.shape(grid)) == 1:
            assert len(grid) == len(energies)
            for i, e in enumerate(energies):
                print(grid[i], e, file=pf)
        elif len(np.shape(grid)) == 4:
            nij, nik, nct = np.shape(grid)[0:-1]
            assert np.prod((nij, nik, nct)) == len(energies)
            c = 0
            for i in range(nij):
                for j in range(nik):
                    for k in range(nct):
                        print(grid[i,j,k,0], grid[i,j,k,1], grid[i,j,k,2], energies[c], file=pf)
                        c += 1
        else:
            raise Exception('unrecognized shape of grid')
