import pytest
import shutil
import os
import numpy as np
from tabulate.utils import get_interpolation_error, get_rho_max_from_xyz
from tabulate.quip_tools import GAP


def test_2b3beam_init(tg_2b3beam):
    assert tg_2b3beam.elements == ['Ta', 'W']
    assert tg_2b3beam.do_2b == True
    assert tg_2b3beam.do_3b == True
    assert tg_2b3beam.do_eam == True
    assert tg_2b3beam.r2b == pytest.approx([0.0, 5.0])
    assert tg_2b3beam.r3b == pytest.approx([0.1, 4.1])
    assert tg_2b3beam.rho_max == pytest.approx(8.0)
    assert tg_2b3beam.n_eams == 1
    assert tg_2b3beam.n3b == (4, 4, 4)


def test_2b(tg_2b):

    tg_2b.compute_energies(save_data=True)
    for pair in ['TaTa', 'TaW', 'WW']:
        e1 = np.loadtxt(f'out-tab/pair_energies_{pair}.dat')
        e2 = np.loadtxt(f'files_tabulate/2b/pair_energies_{pair}.dat')
        assert e1 == pytest.approx(e2)

    tg_2b.write_potential_files()
    with open('Ta-W.tabgap', 'r') as t1:
        with open('files_tabulate/2b/Ta-W.tabgap', 'r') as t2:
            assert t1.readlines()[1:] == t2.readlines()[1:]

    rmse = get_interpolation_error('files_tabulate/test.xyz', tg_2b.pair_style, tg_2b.pair_coeff,
                                   compute_gap=True, gap_xml=tg_2b.xmlpath)
    assert rmse == pytest.approx({'energy': 0.3628776, 'force': 0.07524343, 'virial': None})
    shutil.rmtree('out-tab')
    os.remove('Ta-W.tabgap')


def test_2b3b(tg_2b3b):

    tg_2b3b.compute_energies(save_data=True)
    for pair in ['TaTa', 'TaW', 'WW']:
        e1 = np.loadtxt(f'out-tab/pair_energies_{pair}.dat')
        e2 = np.loadtxt(f'files_tabulate/2b3b/pair_energies_{pair}.dat')
        assert e1 == pytest.approx(e2)
    for triplet in ('TaTaTa', 'TaTaW', 'TaWW', 'WTaTa', 'WTaW', 'WWW'):
        e1 = np.loadtxt(f'out-tab/triplet_energies_{triplet}.dat')
        e2 = np.loadtxt(f'files_tabulate/2b3b/triplet_energies_{triplet}.dat')
        assert e1 == pytest.approx(e2)

    tg_2b3b.write_potential_files()
    with open('Ta-W.tabgap', 'r') as t1:
        with open('files_tabulate/2b3b/Ta-W.tabgap', 'r') as t2:
            assert t1.readlines()[1:] == t2.readlines()[1:]

    rmse = get_interpolation_error('files_tabulate/test.xyz', tg_2b3b.pair_style, tg_2b3b.pair_coeff)
    assert rmse == pytest.approx({'energy': 1598.88457, 'force': 3.16439, 'virial': None})
    shutil.rmtree('out-tab')
    os.remove('Ta-W.tabgap')


def test_2b3beam(tg_2b3beam):

    tg_2b3beam.compute_energies(save_data=True)
    for pair in ['TaTa', 'TaW', 'WW']:
        e1 = np.loadtxt(f'out-tab/pair_energies_{pair}.dat')
        e2 = np.loadtxt(f'files_tabulate/2b3beam/pair_energies_{pair}.dat')
        assert e1 == pytest.approx(e2)
        p1 = np.loadtxt(f'out-tab/pair_density_{pair}-1.dat')
        p2 = np.loadtxt(f'files_tabulate/2b3beam/pair_density_{pair}-1.dat')
        assert p1 == pytest.approx(p2)
    for triplet in ('TaTaTa', 'TaTaW', 'TaWW', 'WTaTa', 'WTaW', 'WWW'):
        e1 = np.loadtxt(f'out-tab/triplet_energies_{triplet}.dat')
        e2 = np.loadtxt(f'files_tabulate/2b3beam/triplet_energies_{triplet}.dat')
        assert e1 == pytest.approx(e2)
    for e in ('Ta', 'W'):
        e1 = np.loadtxt(f'out-tab/embedding_energies_{e}-1.dat')
        e2 = np.loadtxt(f'files_tabulate/2b3beam/embedding_energies_{e}-1.dat')
        assert e1 == pytest.approx(e2)

    tg_2b3beam.write_potential_files()
    with open('Ta-W.tabgap', 'r') as t1:
        with open('files_tabulate/2b3beam/Ta-W.tabgap', 'r') as t2:
            assert t1.readlines()[1:] == t2.readlines()[1:]
    with open('Ta-W.eam.fs', 'r') as t1:
        with open('files_tabulate/2b3beam/Ta-W.eam.fs', 'r') as t2:
            assert t1.readlines()[1:] == t2.readlines()[1:]

    rmse = get_interpolation_error('files_tabulate/test.xyz', tg_2b3beam.pair_style, tg_2b3beam.pair_coeff)
    assert rmse == pytest.approx({'energy': 1148.80227, 'force': 1.2856698, 'virial': None})
    shutil.rmtree('out-tab')
    os.remove('Ta-W.tabgap')
    os.remove('Ta-W.eam.fs')


def test_2beam(tg_2beam):

    tg_2beam.compute_energies(save_data=True)
    for pair in ['TaTa', 'TaW', 'WW']:
        e1 = np.loadtxt(f'out-tab/pair_energies_{pair}.dat')
        e2 = np.loadtxt(f'files_tabulate/2beam/pair_energies_{pair}.dat')
        assert e1 == pytest.approx(e2)
        p1 = np.loadtxt(f'out-tab/pair_density_{pair}-1.dat')
        p2 = np.loadtxt(f'files_tabulate/2beam/pair_density_{pair}-1.dat')
        assert p1 == pytest.approx(p2)
    for e in ('Ta', 'W'):
        e1 = np.loadtxt(f'out-tab/embedding_energies_{e}-1.dat')
        e2 = np.loadtxt(f'files_tabulate/2beam/embedding_energies_{e}-1.dat')
        assert e1 == pytest.approx(e2)

    tg_2beam.write_potential_files()
    with open('Ta-W.eam.fs', 'r') as t1:
        with open('files_tabulate/2beam/Ta-W.eam.fs', 'r') as t2:
            assert t1.readlines()[1:] == t2.readlines()[1:]

    rmse = get_interpolation_error('files_tabulate/test.xyz', tg_2beam.pair_style, tg_2beam.pair_coeff,
                                   compute_gap=True, gap_xml=tg_2beam.xmlpath)
    assert rmse == pytest.approx({'energy': 1.17321777, 'force': 0.004885779, 'virial': None})
    shutil.rmtree('out-tab')
    os.remove('Ta-W.eam.fs')


def test_2bneam(tg_2bneam):

    tg_2bneam.compute_energies(save_data=True)
    for pair in ['TaTa', 'TaW', 'WW']:
        e1 = np.loadtxt(f'out-tab/pair_energies_{pair}.dat')
        e2 = np.loadtxt(f'files_tabulate/2bneam/pair_energies_{pair}.dat')
        assert e1 == pytest.approx(e2)
        for i in range(3):
            p1 = np.loadtxt(f'out-tab/pair_density_{pair}-{i+1}.dat')
            p2 = np.loadtxt(f'files_tabulate/2bneam/pair_density_{pair}-{i+1}.dat')
            assert p1 == pytest.approx(p2)
    for e in ('Ta', 'W'):
        for i in range(3):
            e1 = np.loadtxt(f'out-tab/embedding_energies_{e}-{i+1}.dat')
            e2 = np.loadtxt(f'files_tabulate/2bneam/embedding_energies_{e}-{i+1}.dat')
            assert e1 == pytest.approx(e2)

    tg_2bneam.write_potential_files()
    for i in range(1, 4):
        with open(f'Ta-W-{i}.eam.fs', 'r') as t1:
            with open(f'files_tabulate/2bneam/Ta-W-{i}.eam.fs', 'r') as t2:
                assert t1.readlines()[1:] == t2.readlines()[1:]

    rmse = get_interpolation_error('files_tabulate/test.xyz', tg_2bneam.pair_style, tg_2bneam.pair_coeff,
                                   compute_gap=True, gap_xml=tg_2bneam.xmlpath)
    assert rmse == pytest.approx({'energy': 0.253534664, 'force': 0.004479445, 'virial': None})
    shutil.rmtree('out-tab')
    os.remove('Ta-W-1.eam.fs')
    os.remove('Ta-W-2.eam.fs')
    os.remove('Ta-W-3.eam.fs')


def test_set_n(tg_2b3beam):

    tg_2b3beam.compute_energies(save_data=False)
    assert len(tg_2b3beam.energies_2b[('Ta', 'W')]) == 100
    assert len(tg_2b3beam.energies_3b[('Ta', 'W', 'W')]) == 216
    assert len(tg_2b3beam.energies_eam[0]['Ta']) == 100

    # should recompute 3b and keep 2b and eam energies
    tg_2b3beam.set_n3b((3, 3, 3))
    assert tg_2b3beam.n3b == (3, 3, 3)
    tg_2b3beam.compute_energies(save_data=False)
    assert len(tg_2b3beam.energies_2b[('Ta', 'W')]) == 100
    assert len(tg_2b3beam.energies_3b[('Ta', 'W', 'W')]) == 125
    assert len(tg_2b3beam.energies_eam[0]['Ta']) == 100
    e3b_old = tg_2b3beam.energies_3b[('Ta', 'W', 'W')].copy()

    # should recompute 2b & eam and keep 3b
    tg_2b3beam.set_n2b(30)
    assert tg_2b3beam.n2b == 30
    assert tg_2b3beam.neam == 30
    tg_2b3beam.compute_energies(save_data=False)
    assert len(tg_2b3beam.energies_2b[('Ta', 'W')]) == 30
    assert len(tg_2b3beam.energies_3b[('Ta', 'W', 'W')]) == 125
    assert tg_2b3beam.energies_3b[('Ta', 'W', 'W')] == pytest.approx(e3b_old)
    assert len(tg_2b3beam.energies_eam[0]['Ta']) == 30

    # should recompute 2b & eam and keep 3b
    tg_2b3beam.set_neam(35)
    assert tg_2b3beam.n2b == 35
    assert tg_2b3beam.neam == 35
    tg_2b3beam.compute_energies(save_data=False)
    assert len(tg_2b3beam.energies_2b[('Ta', 'W')]) == 35
    assert len(tg_2b3beam.energies_3b[('Ta', 'W', 'W')]) == 125
    assert tg_2b3beam.energies_3b[('Ta', 'W', 'W')] == pytest.approx(e3b_old)
    assert len(tg_2b3beam.energies_eam[0]['Ta']) == 35
    e2b_old = tg_2b3beam.energies_2b[('W', 'W')].copy()
    eeam_old = tg_2b3beam.energies_eam[0]['Ta'].copy()

    # should recompute eam and keep 2b and 3b
    tg_2b3beam.set_rho_max(1)
    assert tg_2b3beam.rho_max == 1
    tg_2b3beam.compute_energies(save_data=False)
    assert len(tg_2b3beam.energies_2b[('Ta', 'W')]) == 35
    assert len(tg_2b3beam.energies_3b[('Ta', 'W', 'W')]) == 125
    assert len(tg_2b3beam.energies_eam[0]['Ta']) == 35
    assert tg_2b3beam.energies_2b[('W', 'W')] == pytest.approx(e2b_old)
    assert tg_2b3beam.energies_eam[0]['Ta'] != pytest.approx(eeam_old)


def test_get_rho_max():

    gap = GAP('../examples/test_files/gap_W-Ta_example_2b+n_eam.xml')
    rho_max = get_rho_max_from_xyz('files_tabulate/test.xyz', gap)
    assert rho_max == pytest.approx(6.26527364)

    gap = GAP('../examples/test_files/gap_W-Ta_example_2b+eam.xml')
    rho_max = get_rho_max_from_xyz('files_tabulate/test.xyz', gap)
    assert rho_max == pytest.approx(1.428726559)

    gap = GAP('../examples/test_files/gap_W-Ta_example_2b+3b+eam.xml')
    rho_max = get_rho_max_from_xyz('files_tabulate/test.xyz', gap)
    assert rho_max == pytest.approx(1.428726559)
